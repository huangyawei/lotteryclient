cc.Class({
    extends: cc.Component,

    properties: {
        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        pfBaseItem:{
            default:null,
            type:cc.Prefab
        },

        pfBase1Item:{
            default:null,
            type:cc.Prefab
        },

        fbHotItem:{
            default:null,
            type:cc.Prefab
        },

        //开奖界面
        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        //走势界面
        ndBasePanle:{
            default:null,
            type:cc.Node
        },
        
        //冷热界面
        ndHotContent:{
            default:null,
            type:cc.Node
        },

        //形态界面
        ndFormContent:{
            default:null,
            type:cc.Node
        },

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        togSort: [cc.Toggle],

        _frontTg:null,
        _data:null,
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _openItemArr: [],
        _openItemArr2: [],
        _addType: false
    },

    // use this for initialization
    onLoad: function () {
        this.showPage(this._data);
    },

    showPage:function(info){
        this._color = [];
        var color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        
        this._openData = trdata;
       
        this._openData2 =this._openData.slice(0);  //深复制下

        //----
        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =730;
    
        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                sum:this._openData[i].s,
                span:this._openData[i].sp,
                repeat:this._openData[i].d,
                color:this._color[i%2],
                type:0
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data);
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
            
            //形态
            var fromItem = cc.instantiate(this.pfOpenItem);
            fromItem.setPosition(0,-fromItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData2[i].i,
                openNums:this._openData2[i].n,
                sum:this._openData2[i].x[0],
                span:this._openData2[i].x[1],
                repeat:this._openData2[i].x[2],
                color:this._color[i%2],
                type:0
            }
            fromItem.getComponent(fromItem.name).onItemIndex(i);
            fromItem.getComponent(fromItem.name).init(data);
            this.ndFormContent.addChild(fromItem);
            this._openItemArr2.push(fromItem);
        }
    
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;

        this.fromItemHeight =fromItem.height;
        this.ndFormContent.height =this._totalCount*(this.fromItemHeight+this._spacing)+this._spacing;

        this.baseItemArr =[];
        this.base1ItemArr =[];
        for(var i=0;i<this._openData.length;i++)
        {
            //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                nums:this._openData[i].t,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr.push(baseItem);
        }

        //冷热
        var oData = tempinfo["o"];
        var o3Data = tempinfo["o3"];
        var o5Data = tempinfo["o5"];
        var o0Data = tempinfo["o0"];
        this.hotData= [];
        for(var i=1;i<12;i++)
        {
            //冷热
            var data = {
                num:i>9?i:"0"+i,
                issus30:o3Data[i-1],
                issue50:o5Data[i-1],
                issue0:o0Data[i-1],
                miss:oData[i-1],
                color:color[i%2]
            }
  
            this.hotData.push(data);
            var hotItem = cc.instantiate(this.fbHotItem);
            hotItem.getComponent(hotItem.name).init(data);
            this.ndHotContent.addChild(hotItem);
        }
        //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现次数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];

        var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        var snData = tempinfo["sn"];
        var saData = tempinfo["sa"];
        var smData = tempinfo["sm"];
        var ssData = tempinfo["ss"];
        var snamsList = [];
        snamsList.push(snData);
        snamsList.push(saData);
        snamsList.push(smData);
        snamsList.push(ssData);

        for(var i=0;i<4;i++)
        {
            //走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[i%2],
                dec:censusStr[i],
                nums:bnamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.base1ItemArr.push(base1Item);
        }
    },

    init:function(data){
        this._data = data;
    },

    //开奖期号排序
    issueSort:function(toggle){
        var children = this.ndOpenPanle.children;
        var childrenLen = children.length;
        //------
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                sum:this._openData[i].s,
                span:this._openData[i].sp,
                repeat:this._openData[i].d,
                color:this._color[i%2],
                type:0
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    //形态期号排序
    issueByFromSort:function(toggle){
        var children = this.ndFormContent.children;
        var childrenLen = children.length;
        //------
        this.ndFormContent.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked ){
            this._openData2 =this._openData2.reverse(); 
        }
        else{
            this._openData2 =this._openData2.reverse();
        }
       
        for(var i=0;i<this._openItemArr2.length;++i){
            this._openItemArr2[i].setPosition(0,- this._openItemArr2[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData2[i].i,
                openNums:this._openData2[i].n,
                sum:this._openData2[i].x[0],
                span:this._openData2[i].x[1],
                repeat:this._openData2[i].x[2],
                color:this._color[i%2],
                type:0
            }
            this._openItemArr2[i].getComponent(this._openItemArr2[i].name).onItemIndex(i);
            this._openItemArr2[i].getComponent(this._openItemArr2[i].name).updateData(data); 
        }
    },

    //冷热排序
    onAfterSort:function(toggle,customEventData){
        this.hotSort(toggle,this.ndHotContent,this.hotData,customEventData);
    },

    hotSort:function(toggle,panle,arry,key){
        if(arry == null)
            return;
        toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        if(this._frontTg!=null)
            this._frontTg.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        this._frontTg = toggle;

        var children = panle.children;
        var childrenLen = children.length;
        
        var keyStr = "";
        switch (key)
        {
            case "1"://号码
            {
                if(toggle.getComponent(cc.Toggle).isChecked)    
                {
                    Utils.sortByKey(arry,"num",true);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updataData(arry[i]);
                        }
                    }
                }
                else
                {
                    Utils.sortByKey(arry,"num",false);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updataData(arry[i]);
                        }
                    }
                    toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[2];
                }
                return;
            }
            break;
            case "2"://30期
            {
                keyStr = "issue30";
            }
            break;
            case "3"://50期
            {
                keyStr = "issue50";
            }
            break;
            case "4"://100期
            {
                keyStr = "issue0";
            }
            break;
            case "5"://遗漏
            {
                keyStr = "miss";
            }
            break;
        }

        if(keyStr != "")
        {
            if(toggle.getComponent(cc.Toggle).isChecked)    
            {
                Utils.sortByKey(arry,keyStr,false);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updataData(arry[i]);
                    }
                }
                
            }
            else
            {
                Utils.sortByKey(arry,keyStr,true);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updataData(arry[i]);
                    }
                }
                toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[1];
            }
        } 
    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{
            if(this.togSort[0].isChecked){
                Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'115_opencontent_item',0); 
            }else if(this.togSort[3].isChecked){
                //形态 
              Utils.preNodeComplex(this.ndFormContent,this.fromItemHeight,this._spacing,this._reaCount,this._openItemArr2,this.buffer,this._openData2,this._color,'115_opencontent_item',4); 
            }
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //走势
    tgBaseTrend: function(){
        if(this._addType){return}
        for(var i=0;i<this.baseItemArr.length;++i){
            this.ndBasePanle.addChild(this.baseItemArr[i]);  
            this._addType =true; 
        }
        for(var i=0;i<this.base1ItemArr.length;++i){
            this.ndBasePanle.addChild(this.base1ItemArr[i]);
            this._addType =true;    
        }    
    }

});
