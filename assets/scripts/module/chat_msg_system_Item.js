/**
 * !#zh 系统聊天组件
 * @information 系统消息
 */
cc.Class({
    extends: cc.Component,

    properties: {
        rtTxt:{
            default: null,
            type: cc.RichText
        },
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            
            this.rtTxt.string = this._data.rtContent;
            var width = this.rtTxt._linesWidth.length>1 ? 830:this.rtTxt._linesWidth[0];
            var height = this.rtTxt._labelHeight;
            this.node.width += width;
            this.node.height = height+30;
 
            if(this.rtTxt._linesWidth.length > 1)
            {
                this.rtTxt.horizontalAlign = cc.TextAlignment.LEFT;
            }
            else
            {
                this.rtTxt.horizontalAlign = this._data.type;
            }
           

            // this.rtTxt.string = this._data.rtContent;
            // if(this.rtTxt.node.width < 829)
            // {
            //     this.rtTxt.horizontalAlign = cc.TextAlignment.CENTER;
            //     this.node.width = this.rtTxt.node.width;
            //     this.node.height = this.rtTxt.node.height+30;
            // }
            // else
            // {
            //     this.rtTxt.horizontalAlign = cc.TextAlignment.LEFT;
            //     this.rtTxt.node.lineHeight = 
            //     this.node.width = this.rtTxt.node.width;
            //     this.node.height = this.rtTxt.node.height+30;
            // }
        }
    },

    /** 
    * 接收系统消息数据
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
