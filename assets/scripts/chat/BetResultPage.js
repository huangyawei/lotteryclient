cc.Class({
    extends: cc.Component,

    properties: {
        resultContent:{
            default: null,
            type: cc.Layout
        },
        publicReward:{
            default: null,
            type: cc.Prefab
        },

        ballsPrefab:{
            default: [],
            type: cc.Prefab
        },
        
        scrollview:{
            default:null,
            type:cc.ScrollView
        },

       betIsuseDetails:{
            default: null,
            type: cc.Prefab
        },

        labListName:{
            default: null,
            type: cc.Label
        },
        
        _lotteryId:0,
        _curPageIndex:1,
        _curLineIndex:11,
        _curPrefab:null,
        _isNowRefreshing:false
    },

    // use this for initialization
    onLoad: function () {
        this.initPrefab();
        this._isNowRefreshing = false;
        this.getResult();
        window.Notification.on("closeBetResult",function(data){
            this.onClose();
        },this);
    },

    init: function(ID){
        this._lotteryId = ID;
        this._curPageIndex = 1;
        this._curLineIndex = 11;
        this.labListName.string = LotteryUtils.getLotteryTypeDecByLotteryId(this._lotteryId) + "-开奖列表";
    },

    getIsuseAndTime:function(timeStr,isuse){
        var isuseAndWeekStr = "";
        if(timeStr != ""){
            var hourStr = "";
            var MinutesStr = "";
            var weekStr = "";

            var date = Utils.formateServiceDateStrToDate(timeStr);
            switch (date.getDay()) {
                case 0:weekStr="星期天";break
                case 1:weekStr="星期一";break
                case 2:weekStr="星期二";break
                case 3:weekStr="星期三";break
                case 4:weekStr="星期四";break
                case 5:weekStr="星期五";break
                case 6:weekStr="星期六";break
            }
            hourStr = (date.getHours()).toString();
            hourStr = hourStr.length==1?"0"+hourStr:hourStr;

            MinutesStr = (date.getMinutes()).toString();
            MinutesStr = MinutesStr.length==1?"0"+MinutesStr:MinutesStr;

            isuseAndWeekStr = "第"+isuse+"期  "+hourStr+ " : " +MinutesStr+" ("+weekStr+")";
        }else{
            isuseAndWeekStr = "第"+isuse+"期";
        }
        return isuseAndWeekStr;
    },

    getResult:function(){
        var recv = function(ret){
            if(ret.Code !== 0)
            {
                ComponentsUtils.showTips(ret.Msg);
                cc.error(ret.Msg);
            }
            else
            {
                var data = ret.Data;
                for(var i = 0;i<data.length;i++)
                {
                    var spriteNums = [];
                    var sumValue = 0;
                    var stateStr = "";
                    var petNumberArray = [];
            
                    if(this._lotteryId == "101" || this._lotteryId == "102")
                    {
                        if(data[i].Number != "" )
                        {
                            petNumberArray = data[i].Number.split(" ");
                            for(var j=0;j<petNumberArray.length;j++)
                            {
                                var value = parseInt(petNumberArray[j]);
                                spriteNums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(value)); 
                                sumValue += value;
                            }
                        }
                        else
                        {
                            var sp0 = CL.MANAGECENTER.getDiceSpriteFrameByNum(0);
                            spriteNums.push(sp0);
                            spriteNums.push(sp0);
                            spriteNums.push(sp0);
                        }
                        sumValue = isNaN(sumValue)==true?0:sumValue;
                        if(sumValue != 0){
                            if(sumValue<=10){
                                stateStr = "小";
                            }else{
                                stateStr = "大";
                            }
                            if(sumValue%2 == 0){
                                stateStr += "双";
                            }else{
                                stateStr += "单";
                            }   
                        }
                        stateStr = "和值："+ sumValue.toString() + "    形态：" + stateStr;
                        var isuseTime = this.getIsuseAndTime(data[i].OpenTime,data[i].IsuseNum);
                        var reward = cc.instantiate(this.publicReward);
                        reward.getComponent('bet_publicResult_Item').init({
                            msg: stateStr,
                            isuse: isuseTime,
                            ballFrames:spriteNums,
                            ballNums:null,
                            ballPrefab:this._curPrefab
                        });

                        var details = {
                            isusenum:data[i].IsuseNum,
                            isuse: isuseTime,
                            ballFrames:petNumberArray,
                            ballNums:null,
                        };
                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = this.node; 
                        clickEventHandler.component = "BetResultPage"
                        clickEventHandler.handler = "onClickCallBack";
                        clickEventHandler.customEventData = details;
                        reward.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                        this.resultContent.node.addChild(reward); 
                    }
                    else
                    {
                        if(data[i].Number != "" )
                        {
                            petNumberArray = data[i].Number.split(" ");
                            for(var j=0;j<petNumberArray.length;j++)
                            {
                                spriteNums.push(petNumberArray[j]); 
                            }
                        }
                        var isuseTime = this.getIsuseAndTime(data[i].OpenTime,data[i].IsuseNum);
                        var reward = cc.instantiate(this.publicReward);
                        reward.getComponent('bet_publicResult_Item').init({
                            msg: stateStr,
                            isuse: isuseTime,
                            ballFrames:null,
                            ballNums:spriteNums,
                            ballPrefab:this._curPrefab
                        });

                        var details = {
                            isusenum:data[i].IsuseNum,
                            isuse: isuseTime,
                            ballFrames:null,
                            ballNums:spriteNums,
                        };
                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = this.node; 
                        clickEventHandler.component = "BetResultPage"
                        clickEventHandler.handler = "onClickCallBack";
                        clickEventHandler.customEventData = details;
                        reward.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                        this.resultContent.node.addChild(reward); 
                    }
                }
                this._curPageIndex++
            }
            ComponentsUtils.unblock();
            this._isNowRefreshing = false;
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._lotteryId,
            PageNumber:this._curPageIndex,
            RowsPerPage:this._curLineIndex,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETTHELOTTERYLIST, data, recv.bind(this),"POST");     
        ComponentsUtils.block();
    },

    initPrefab:function(){
        var type = this._lotteryId.substr(0,1);
        this._curPrefab = null;
        var index = 0;
        switch (parseInt(type))
        {
            case 1://k3
            {
               index = 1;
            }
            break;
            case 2://115
            {  
                index = 2;
            }
            break;
            case 3://ssc
            {
                index = 3;
            }
            break;
            case 8://ssq
            {
                index = 4;
            }
            break;
            case 9://big
            {
                index = 5;
            }
            break;    

        }
        this._curPrefab = this.ballsPrefab[index];
    },

    onClickCallBack:function(event, customEventData)
    {
        this.isuseTemp = customEventData;
        var recv = function(ret){
            if(ret.Code !== 0)
            {
                ComponentsUtils.showTips(ret.Msg);
                cc.error(ret.Msg);
            }
            else
            {
                var data = ret.Data;
                var page = cc.instantiate(this.betIsuseDetails);
                page.getComponent(this.betIsuseDetails.name).init({
                    ID:this._lotteryId,
                    isuseData:this.isuseTemp,
                    revData:data,
                });
                var canvas = cc.find("Canvas");
                canvas.addChild(page);
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._lotteryId,
            IsuseNum:this.isuseTemp.isusenum
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETTHELOTTERYISUSE, data, recv.bind(this),"POST");   
    },

    scrollCallBack: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
            var offset_y = this.scrollview.getScrollOffset().y;
            var max_y = this.scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                if(this._isNowRefreshing == false){
                    this._isNowRefreshing = true;
                    this.getResult(this._lotteryId, false);     
                } 
            }
        }
    },

    onClose:function(){
        window.Notification.offType("closeBetResult");
        this.node.getComponent("Page").backAndRemove();
    }

});
