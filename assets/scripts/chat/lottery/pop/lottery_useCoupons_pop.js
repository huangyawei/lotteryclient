cc.Class({
    extends: cc.Component,

    properties: {
        ndContent:cc.Node,
        fdCouponsItem:cc.Prefab,
        _callback:null,
        _data:null,
        _selIndex:-1,
        _couponsID:0,
        _tgSels:[],
    },

    // use this for initialization
    onLoad: function () {
        //this._selIndex = -1;
        for(var i=0;i<this._data.length;i++)
        {
            var decStr = "";
            var moneyStr = "";
            var timeStr = "";
            var rangeStr = "";
            var ischeck = false;
            if(this._data[i].CouponsType == 2)
            {
                decStr = "满" + LotteryUtils.moneytoServer(this._data[i].SatisfiedMoney) + "减" + LotteryUtils.moneytoServer(this._data[i].FaceValue);
            }
            else
            {
                decStr = LotteryUtils.moneytoServer(this._data[i].FaceValue);
            }

            var mony = LotteryUtils.moneytoServer(this._data[i].Balance);
            moneyStr = "余额：" + mony + "元";
            var startStr = Utils.getYMDbyTime(this._data[i].StartTime,"/"); 
            var endStr = Utils.getYMDbyTime(this._data[i].ExpireTime,"/"); 
            timeStr = "有效期：" +startStr + " ~ " + endStr;

            if(this._data[i].LotteryCode == 0)
            {
                rangeStr = "范 围：全场通用";
            }
            else
            {
                rangeStr = "范 围：" + LotteryUtils.getLotteryTypeDecByLotteryId(this._data[i].LotteryCode);
            }

            if(this._couponsID <= 0 )
            {
                if(this._couponsID = -1)
                {
                    this._selIndex = -1;
                    ischeck = false;
                }   
                else
                {
                    
                    if(i==0)
                    {
                        this._selIndex = 0;
                        ischeck = true;
                    }
                    else
                        ischeck = false;      
                }    
            }
            else
            {
                if(this._couponsID == this._data[i].CouponsID)
                {
                    this._selIndex = i;
                    ischeck = true;
                }
                else
                    ischeck = false;  
            }

            var fpCoupons = cc.instantiate(this.fdCouponsItem);
            var data = {
                dec:decStr,
                money:moneyStr,
                time:timeStr,
                range:rangeStr,
                isCheck:ischeck
            }
            fpCoupons.getComponent(fpCoupons.name).init(data);

            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "lottery_useCoupons_pop";
            checkEventHandler.handler = "onClickCallBack";
            checkEventHandler.customEventData ={couponsID:this._data[i].CouponsID,couponsMoney:mony,index:i};
            fpCoupons.getChildByName("toggle").getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
            this.ndContent.addChild(fpCoupons);
            this._tgSels.push(fpCoupons);
        }
    },

    init:function(data,couponsid,callback){
        this._callback = null;
        this._callback = callback;
        this._couponsID = couponsid;
        this._data = data;
    },

    onClickCallBack:function(toggle, customEventData){  
        if(customEventData.index != this._selIndex)
        {
            if(this._selIndex != -1)
                this._tgSels[this._selIndex].getChildByName("toggle").getComponent(cc.Toggle).isChecked = false; 
            this._selIndex = customEventData.index;
        }
        else
        {
            customEventData.couponsID = -1;
            customEventData.couponsMoney = 0;
        }
        if(this._callback !=null)
        {
            var data = {
                couponsID:customEventData.couponsID,
                couponsMoney:customEventData.couponsMoney
            }
            this._callback(data);
        }
        this.onClose();
    },
    
    //关闭
    onClose:function(event){
        this.node.getComponent("Page").backAndRemove();
    },
});
