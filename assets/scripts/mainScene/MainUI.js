/*
大厅界面控制
*/
cc.Class({
    extends: cc.Component,

    properties: {
        outHall: cc.Node,
        outUser: cc.Node,
        outOpen: cc.Node,
        seletOut: cc.Node,

        toggleGroup:{
            default: [],
            type: cc.Toggle
        },
        curToggle:{
            default: null,
            type: cc.Toggle
        }, 
   
        ndwin:{
            default:null,
            type:cc.Node
        },

        labWin1:{
            default:null,
            type:cc.Label
        },

        labWin2:{
            default:null,
            type:cc.Label
        },

        win1:cc.Animation,
        win2:cc.Animation,
        fbShare:cc.Prefab,
        outNotice:cc.Node,
        spBgAin:cc.Node,
        mainAin:cc.Node,
    },
    
    // use this for initialization
    onLoad: function () {
        window.Notification.on("onGotoHall",function(data){
            if(this.toggleGroup[0].getComponent(cc.Toggle) !== this.seletOut.getComponent(cc.Toggle))
            {
                this.toggleGroup[0].getComponent(cc.Toggle).isChecked = true;
                this.seletOut.getComponent(cc.Toggle).isChecked = false;
            }
            this.onGotoHall(this.toggleGroup[0],"");
        },this);

        window.Notification.on("onGotoUser",function(data){
            if(this.toggleGroup[4].getComponent(cc.Toggle) !== this.seletOut.getComponent(cc.Toggle))
            {
                this.toggleGroup[4].getComponent(cc.Toggle).isChecked = true;
                this.seletOut.getComponent(cc.Toggle).isChecked = false;
            }
            
            this.onGotoUser(this.toggleGroup[4],"");
        },this);

        window.Notification.on("playWin",function(data){
            if(this.isMainScence())
            {
                var animState1 = this.win1.getAnimationState('ani_win');
                var animState2 = this.win2.getAnimationState('ani_win_rotation');
                if(animState1.isPlaying || animState2.isPlaying || this.ndwin.active)
                    return;
                this.playWin(data);
            }
        },this);

        this.ndwin.on(cc.Node.EventType.TOUCH_START, function (event) {
            //this.node.dispatchEvent.call(this, event);
            console.log('playwin TOUCH_START-' + this.ndwin.name);  
        }.bind(this), this);

    },

    start:function(){
        //this.node.emit("fade-in"); 
    },

    isMainScence:function(){
        var canvas = cc.find("Canvas");
        var canvasChildren = canvas.children;
        var highestZOrder = 0;
        highestZOrder = canvasChildren[canvasChildren.length-1].getLocalZOrder();
        if(highestZOrder == 0)
            return true;
        else
            return false;    
    },

    //大厅
    onGotoHall: function(toggle, customEventData){ 
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this.seletOut = toggle;
            this.outNotice.active = true;
            this.outUser.getComponent("UserUI").deleteOrder();
        }
    },

    //擂台
    onGotoArena: function(toggle, customEventData){
        this.curToggle.getComponent(cc.Toggle).isChecked = true;
        toggle.getComponent(cc.Toggle).isChecked = false;
        ComponentsUtils.showTips("擂台暂未开放");         
    },

    //游戏
    onGotoGame: function(toggle, customEventData){
        this.curToggle.getComponent(cc.Toggle).isChecked = true;
        toggle.getComponent(cc.Toggle).isChecked = false;
        ComponentsUtils.showTips("游戏暂未开放");  
    },

    //圈子
    onGotoCircle: function(toggle, customEventData){
        this.curToggle.getComponent(cc.Toggle).isChecked = true;
        toggle.getComponent(cc.Toggle).isChecked = false;
        ComponentsUtils.showTips("圈子暂未开放");  
    },
    
    //中奖
    onGotoOpen: function(toggle, customEventData){
        this.seletOut = toggle;
        this.outNotice.active = true;
        this.outOpen.getComponent("OpenUI").getResult(false);
    },
    
    //我
    onGotoUser: function(toggle, customEventData){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {     
            if(User.getIsvermify())//验证通过
            {
                this.seletOut = toggle;
                toggle.getComponent(cc.Toggle).isChecked = true;
                this.outNotice.active = false;
                this.outUser.getComponent("UserUI").resetRefresh();    
            }
            else
            {
                var self = this;
                var callback = function callback(ret) {
                    if(ret && ret == true)
                    {
                        this.seletOut = toggle;
                        this.outNotice.active = false;
                        self.outUser.getComponent("UserUI").resetRefresh();
                    }
                    else
                    {
                        this.seletOut = this.toggleGroup[0];
                        toggle.getComponent(cc.Toggle).isChecked = false;
                        this.toggleGroup[0].getComponent(cc.Toggle).isChecked = true;                        
                    }   
                }
                CL.MANAGECENTER.gotoLoginPop(callback.bind(this));
            }
        }
    },

    playWin:function(data){
        if(data.length==3)
        {
           // cc.log("playwin ok");
            this.ndwin.active = true;
            this.labWin1.string = data[1];
            this.labWin2.string = data[2];   
             //this.node.runAction(cc.sequence(cc.delayTime(1), cc.callFunc(function(){
                this.onShowAin(true);
             //}.bind(this))));
            
        }
    },

    onCloseAni:function(){
        //cc.log("playwin close");
        this.onShowAin(false);
    },

    onShowAin:function(isactive){
        if(isactive)
        {
            this.ndwin.active = true;
            this.win1.play('ani_win');
            this.win2.play('ani_win_rotation');
        }
        else
        {
            this.ndwin.active = false;
            this.win1.play('ani_win');
            this.labWin1.string = "";
            this.labWin2.string = "";
            this.win2.play('ani_win_rotation');
        }
    },

    shareWin:function(flag){
        ComponentsUtils.showTips("暂时不支持微信分享！");
    },

    onDestroy: function(){
        window.Notification.offType("onGotoHall");
        window.Notification.offType("onGotoUser");
        window.Notification.offType("playWin");
    }

});
