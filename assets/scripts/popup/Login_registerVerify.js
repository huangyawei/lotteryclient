/*
注册验证界面
*/
cc.Class({
    extends: cc.Component,

    properties: {
        edPassword: {
            default: null,
            type: cc.EditBox
        },

        labDec: {
            default: null,
            type: cc.Label
        },

        labDecTime:{
            default: null,
            type: cc.Label
        },

        _account:"",
        _password:"",
        _bgetVerity:true,
        intervalTime: 60
    },

    // use this for initialization
    onLoad: function () {

    },

    initData: function(data){
        this._account = data.account;
        this._password = data.password;
    },

    onTextChangedVerify:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.verify = txt;
        else
        {
            txt = this.verify;
            editbox.string = txt;
            return;
        }
    },

    onTrue: function(){
         if(Utils.checkInput(null,null,this.edPassword.string,null,true))
         {
            ComponentsUtils.block();
            var self = this;
            var recv = function(ret){
                ComponentsUtils.unblock();
                if (ret.Code !== 0 && ret.Code !== 100) {
                    cc.error(ret.Msg);
                    ComponentsUtils.showTips(ret.Msg);
                }else{
                    User.setUserCode(ret.Data.UserCode);
                    User.setNickName(ret.Data.Nick);
                    User.setTel(ret.Data.Mobie);
                    User.setBalance(ret.Data.Balance);
                    User.setGoldBean(ret.Data.Gold);
                    User.setIsrobot(ret.Data.IsRobot);
                    User.setAvataraddress(ret.Data.AvatarUrl);
                    User.setVipLevel(ret.Data.VIP);
                    User.setFullName(ret.Data.FullName);
                    User.setIsCertification(ret.Data.IsCertification);
                    User.setLoginToken(ret.Data.Token);
                    User.setPwd(this._password);
                    User.setIsvermify(true);//验证通过
                    ComponentsUtils.showTips("注册成功!");
                    if(ret.Code !== 100)
                    {
                        CL.NET.login();
                    }
                        
           
                    window.Notification.emit("onCloseLogin",true);
                    this.onClose();
                }
            }.bind(this);

            var data = {
                Equipment: Utils.getEquipment(),
                SourceType: Utils.getSourceType(),
                Mobile: this._account,
                Pwd: this._password,
                VerifyCode:this.edPassword.string,
            };
            CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.REGISTER, data, recv.bind(this),"POST");  
         }
    },

    _updateVerify: function(){
        this.intervalTime = this.intervalTime-1;
        this.labDec.string = this.intervalTime+"秒后重发";
        if(this.intervalTime <= 0){
            this.intervalTime = 60;
            this._bgetVerity = true;
            this.unschedule(this._updateVerify);
            this.labDec.string = "获取验证码"
            this.labDecTime.string ="点击获取验证码，并输入验证码";
        }
    },

    onGetVerifyCode: function(){
        if(!this._bgetVerity)
            return;

        ComponentsUtils.block();
        var self = this;
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                cc.error(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                self._bgetVerity = false;
                ComponentsUtils.showTips("发送成功!");  
                var numberTop = this._account.substring(0,3);
                var numberdown = this._account.substring(this._account.length-4);
                var number = numberTop+"****"+numberdown;
                this.labDecTime.string = "亲爱的彩乐彩票用户，您的注册码已发送到"+number+"手机上,请注意查收";
                self.schedule(self._updateVerify, 1);
            }
        }.bind(this);
        cc.log("发送token"+User.getLoginToken());
        var data = {
            Equipment: Utils.getEquipment(),
            Token: User.getLoginToken(),
            Mobile: this._account,
            VerifyType:DEFINE.SHORT_MESSAGE.REGISTER,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETVERIFY, data, recv.bind(this),"POST");  
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
    
});
