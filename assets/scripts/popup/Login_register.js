/*
* 登录注册界面
*/
cc.Class({
    extends: cc.Component,

    properties: {

        edAccount: {
            default: null,
            type: cc.EditBox
        },

        edPassword: {
            default: null,
            type: cc.EditBox
        },

        tgTrue:{
            default:null,
            type:cc.Toggle
        },

        serPrefab:{
            default: null,
            type: cc.Prefab
        },

        _resultStr:null
    },

    // use this for initialization
    onLoad: function () {
        cc.loader.loadRes('/text/proto', function (error, result) {
            this._resultStr =result;
        }.bind(this));    
    },

    onTextChangedPhone:function(text, editbox, customEventData){
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
            this.phoneNum = txt;
        else
        {
            txt = this.phoneNum;
            editbox.string = txt;
            return;
        }
     },

    onTextChangePwd:function(text, editbox, customEventData){
        var txt = text;
        if(txt == "")
        {
            this.pwd = txt;
        }
        else
        {
            var re = txt.replace(/\s+/g,"");
            editbox.string = re;
            txt = re;
            this.pwd = txt;
        }
    },

    onRegister: function() {
        if(Utils.checkInput(this.edAccount.string,this.edPassword.string,null,null,true))
        {
            if(this.tgTrue.isChecked)
            {   
                this.checkTelToServer();
            }
            else
            {
                ComponentsUtils.showTips("请同意注册协议！");
            }
        }
    },

    checkTelToServer:function(){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if (ret.Code !== 0) {
                cc.error(ret.Msg);
                ComponentsUtils.showTips(ret.Msg);
            }else{
                var ret = {
                    account:this.edAccount.string,
                    password:this.edPassword.string
                };
                window.Notification.emit("onRegisterVerify",ret);
                this.onClose();
            }
        }.bind(this);
        var data = {
            Token: User.getLoginToken(),
            Mobile: this.edAccount.string,
        };
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETVERIFYMOBILE, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onService: function() {
        var confirmCallback = function(){
        }.bind(this);
        ComponentsUtils.showAlertTips(4, "尊敬的会员您好！欢迎来到彩乐彩票。如有疑问请拨打客户热线 020-85200475", null, "联系客服", confirmCallback, "确定");
    },

    //注册协议
    onServer: function(){
        var canvas = cc.find("Canvas");
        var Prefab = cc.instantiate(this.serPrefab);
        //协议标题
        var title =Prefab.getChildByName('content').getChildByName('content').getChildByName('labTopTitle');
        title.getComponent(cc.Label).string ='彩乐用户服务协议';
        var Scroll =Prefab.getChildByName('content').getChildByName('content').getChildByName('ndBottomcontent').getChildByName('scrollview');
        var labDec =Scroll.getComponent(cc.ScrollView).content.getChildByName('labDec').getComponent(cc.Label);
        labDec.string =this._resultStr;
        canvas.addChild(Prefab);
        Prefab.active = true;    
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
    
});
