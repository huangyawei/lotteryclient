/**
 * 密码修改成功
 */
cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // use this for initialization
    onLoad: function () {

    },

    onTrue: function(){
        this.onClose();
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    }
    
});
